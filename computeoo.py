import math
import sys


class Compute:
    def __init__(self, b_e, num):
        self.b_e = b_e
        self.num = num

    def power(self):
        return self.num ** self.b_e

    def log(self):
        return math.log(self.num, self.b_e)


if __name__ == "__main__":

    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        num2 = 2
    else:
        try:
            num2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: third argument should be a number")

    operacion = Compute(num2, num)

    if sys.argv[1] == "power":
        result = operacion.power()
    elif sys.argv[1] == "log":
        result = operacion.log()
    else:
        sys.exit('Operand should be power or log')

    print(result)

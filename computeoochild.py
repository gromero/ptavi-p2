import sys
import computeoo


class ComputeChild(computeoo.Compute):
    def set_def(self, b_e=None):
        if b_e is None:
            return 2
        elif b_e <= 0:
            raise ValueError('Error: third argument should be > 0')
        else:
            return b_e

    def get_def(self, b_e):
        return b_e


if __name__ == "__main__":

    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")
    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")
    opdef = ComputeChild(2.0, num)
    if len(sys.argv) == 3:
        n2 = opdef.set_def()

    else:
        try:
            n2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: third argument should be a number")
        opdef = ComputeChild(n2, num)
        n2 = opdef.set_def(n2)

    if sys.argv[1] == "power":
        result = opdef.power()
    elif sys.argv[1] == "log":
        result = opdef.log()
    else:
        sys.exit('Operand should be power or log')

    print(result)
    print('The value of log bass or power exp is: ', opdef.get_def(n2))

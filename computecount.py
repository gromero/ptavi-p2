import math


class Compute:
    def __init__(self):
        self.count = 0

    def power(self, num, b_e=2):
        self.count = self.count + 1
        return num ** b_e

    def log(self, num, b_e=2):
        self.count = self.count + 1
        return math.log(num, b_e)

    def set_def(self, b_e=2):
        self.count = self.count + 1
        if b_e is None:
            return 2
        elif b_e <= 0:
            raise ValueError('Error: third argument should be > 0')
        else:

            return b_e

    def get_def(self, b_e=2):
        self.count = self.count + 1
        return b_e
